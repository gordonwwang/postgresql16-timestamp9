%global sname timestamp9
%global pgmajorversion 16

Summary:	An efficient nanosecond precision timestamp type for Postgres
Name:		postgresql%{pgmajorversion}-%{sname}
Version:	1.4.0
Release:	1%{?dist}
License:	MIT
URL:		https://github.com/fvannee/%{sname}
Source0:	https://github.com/fvannee/%{sname}/archive/refs/tags/%{sname}-%{version}.tar.gz

BuildRequires:	cmake >= 3.17 gcc gcc-c++
BuildRequires:	postgresql%{pgmajorversion}-private-devel
# pg_config -> pg_server_config (postgresql-server-devel provides)
BuildRequires:	postgresql%{pgmajorversion}-server-devel
Requires:	    postgresql%{pgmajorversion}-server

%description
timestamp9 is an efficient nanosecond precision timestamp type for PostgreSQL.

%prep
%autosetup -n %{sname}-%{sname}-%{version}

%build
%{__mkdir} build
pushd build
cmake3 ..
popd

%install
pushd build
%make_install
popd

%files
%defattr(644,root,root,755)
%{_libdir}/pgsql/%{sname}.so
%{_datadir}/pgsql/extension/%{sname}*.sql
%{_datadir}/pgsql/extension/%{sname}.control

%changelog
* Fri Apr 12 2024 Wang Guodong <gordonwwang@tencent.com> - 1.4.0-1
- init build
